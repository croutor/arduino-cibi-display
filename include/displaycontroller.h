/*
 * Copyright (c) 1998, Vincent Hervieux vincent.hervieux@gmail.com
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 * * Neither the name of the author Vincent Hervieux, nor the
 *   names of its contributors may be used to endorse or promote products
 *   derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef DISPLAYCONTROLLER_H
#define DISPLAYCONTROLLER_H

#include "cibi_channels.h"
#include "cibiview.h"
#include "configview.h"
#include "mainview.h"

class DisplayController
{
public:
  /* Constructor and initiliaze display */
  DisplayController();
  /* De-initiliaze and release display */
  ~DisplayController();
  /* Update info on display */
  void updateChannel(cibi_channel_t *_channel);
  void updateModulation(int _modulation);
  void invertDisplay(boolean i);

private:
  /**
         * CONNEXIONS
         * GND    GND
         * VDD    +5V
         * SCLK   pin A5
         * SDA    pin A4
         */
  /* defining the different display types */
  typedef enum
  {
    DISPLAY_MAIN = 0, /* main menu display: channel and frequency have the same size on the screen */
    DISPLAY_CIBI,     /* the main information is the cibi channel */
    DISPLAY_CONFIG    /* reserved for future usage */
  } display_type_t;
  /* Display initialisation */
  void init();
  /* Update displayed frequency */
  void setFreq(uint32_t _frequency, uint16_t _color);
  /* Update displayed modulation */
  void setModulation(int _modulation, uint16_t _color);
  /* update cibi channel display */
  void setCibiChannel(int _channel, uint16_t _color);
  void setCibiChannelBis(bool _bis, uint16_t _color);
  void setCibiBand(int _band, uint16_t _color);

  // Compute current view
  void setView(display_type_t _view);
  // OLED module
  Adafruit_SSD1306 display_;
  // current displayed values
  bool first_display_;
  /* display context */
  int current_modulation_;
  cibi_channel_t current_cibi_channel_;
  /* different views */
  CibiView cibi_view_;
  //ConfigView config_view_;
  MainView main_view_;
  // current view pointer
  View *view_;
};

#endif /* DISPLAYCONTROLLER_H */
