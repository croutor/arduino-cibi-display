/*
 * Copyright (c) 1998, Vincent Hervieux vincent.hervieux@gmail.com
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 * * Neither the name of the author Vincent Hervieux, nor the
 *   names of its contributors may be used to endorse or promote products
 *   derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef CIBI_CHANNELS_H
#define CIBI_CHANNELS_H

#include <Arduino.h>

/* A cibi band is 45 channels:
  - 40 channels
  - 5 'bis' channels: 3,7,11,15,19  */
const uint16_t CIBI_BAND_DAKHZ PROGMEM = 45;
/* definning a structure to store names of Cibi channels */
typedef struct
{
  uint32_t frequency;
  int8_t channel;
  int8_t band;
  bool bis;
} cibi_channel_t;

/// Structures
typedef struct
{
  int8_t modulation;      /* radio modulation currently in use */
  bool modulation_update; /* modulation has to be rendered on screen */
  bool contrast;          /* display contrast*/
  bool contrast_update;   /* constrat has to be rendered on screen */
  uint16_t pll;           /* current PLL value  */
  int8_t cibi_model;      /* cibi model TODO: changeable value via configuration menu */
  cibi_channel_t channel; /* cibi channel */
  bool channel_update;    /* channel has to be rendered on screen */
} context_t;

/* defining the different modulation types */
enum
{
  MOD_NONE = -1,
  MOD_CW = 0,
  MOD_AM,
  MOD_FM,
  MOD_USB,
  MOD_LSB
};

/* Cibi band enum */
enum
{
  CIBI_BAND_UNKNOWN = -1,
  CIBI_BAND_3_INF = 0,
  CIBI_BAND_2_INF,
  CIBI_BAND_1_INF,
  CIBI_BAND_NORMAL,
  CIBI_BAND_1_SUP,
  CIBI_BAND_2_SUP,
  CIBI_BAND_3_SUP,
  CIBI_BAND_MAX = CIBI_BAND_3_SUP
};

/* Supported Cibi models */
enum
{
  CIBI_MODEL_360FM = 0,
  CIBI_MODEL_3900,
  CIBI_MODEL_JACKSON,
  CIBI_MODEL_GRANT,
  CIBI_MODEL_ALAN88S,
  CIBI_MODEL_UNKNOWN
};

const static uint16_t pll_min_by_model[CIBI_MODEL_UNKNOWN] PROGMEM = {  82,  91,  82,  82,  42};
const static uint16_t pll_max_by_model[CIBI_MODEL_UNKNOWN] PROGMEM = { 216, 361, 307, 126, 286};

/* make a bis for cibi channels 3,7,11,15,19 */
const static int8_t cibi_channels[CIBI_BAND_DAKHZ] PROGMEM = {
    1, 2, 3, 3, 4, 5, 6, 7, 7, 8, 9,
    10, 11, 11, 12, 13, 14, 15, 15, 16, 17, 18, 19, 19,
    20, 21, 22, 24, 25, 23, 26, 27, 28, 29,
    30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
    40};

const static bool cibi_channels_bis[CIBI_BAND_DAKHZ] PROGMEM = {
    /* 1- 9*/ false, false, false, true /*3*/, false, false, false, false, true /*7*/, false, false,
    /*10-19*/ false, false, true /*11*/, false, false, false, false, true /*15*/, false, false, false, false, true /*19*/,
    /*20-29*/ false, false, false, false, false, false, false, false, false, false,
    /*30-39*/ false, false, false, false, false, false, false, false, false, false,
    /* 40  */ false};

#endif // CIBI_CHANNELS_H
