/*
 * Copyright (c) 1998, Vincent Hervieux vincent.hervieux@gmail.com
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 * * Neither the name of the author Vincent Hervieux, nor the
 *   names of its contributors may be used to endorse or promote products
 *   derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef VIEW_H
#define VIEW_H

#include <Arduino.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>
#include "fonts/FreeSansBold24pt7bNumeric.h"
#include "fonts/FreeSansBold18pt7bNumeric.h"
#include "fonts/FreeSansBold12pt7bNumeric.h"

class View
{
protected:
  View(Adafruit_SSD1306 *_display);
  /* only usable by Display class */
  friend class DisplayController;
  /* Display initialisation */
  virtual void init() = 0;
  /* Update displayed frequency */
  virtual void setFreq(char *_frequency, uint16_t _color) = 0;
  /* Update displayed modulation */
  virtual void setModulation(char *_modulation, uint16_t _color);
  /* update cibi channel display */
  virtual void setCibiChannel(char *_channel, uint16_t _color) = 0;
  virtual void setCibiChannelBis(bool _bis, uint16_t _color) = 0;
  virtual void setCibiBand(char *_band, uint16_t _color) = 0;

private:
  // OLED module
  Adafruit_SSD1306 *display_;
};

#endif // VIEW_H
