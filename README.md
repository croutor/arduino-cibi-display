# arduino-cibi-display

Arduino project to replace some common citizen band displays.

## Before:
![before](img/cb_display_before_01.jpg)

## After:
- Display on cibi band:
![after_cb](img/cb_display_after_01.jpg)
- Display on non cibi bands:
![after_dx](img/cb_display_after_02.jpg)

## License:
This software is under the [BSD license](LICENSE.md)

## Supported PLL:
### [MB8719](doc/MB8719.pdf): 
- President Jack
- President [Grant](doc/grant.md)
### PLL02A
- Midland [Alan88s](doc/alan88s.md)
### [MC145106P](doc/MC145106.pdf):
- President/Uniden [Jackson 1 226 channels](doc/jackson1.md)
- President [Superstar 360FM](doc/superstar360fm.md)
- CRT [Superstar 3900](doc/superstar3900.md)
- ...

## Bill Of Materials
-  1 x Arduino nano R3
-  1 x monochrome OLED SSD1306 128x64 0.96" IIC
- 14 x 10k resistor 1/4W
-  1 x mini switch
-  1 x DIP 18 supports
-  1 x 5v voltage regulator 7805
-  2 x 10µF 10V capacitors

## Shematic
![schematic](doc/schematic.png)

## Breadboard
![breadboard](doc/breadboard.png)

