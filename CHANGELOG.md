# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Current version

## 1.1.1 - 2019-08-02
- update images with actual display

## 1.1.0 - 2019-03-23
- bigger display of frequencies

## 1.0.0 - 2019-03-08
- initial creation
