#!/usr/bin/python
#
# Copyright (c) 1998, Vincent Hervieux vincent.hervieux@gmail.com
# All rights reserved.
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# * Neither the name of the author Vincent Hervieux, nor the
#   names of its contributors may be used to endorse or promote products
#   derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

def pll_value_to_str(pll_value):
    pll_str = '{0:09b}'.format(pll_value)
    pll_str = pll_str[0] + ' |  ' +\
              pll_str[1] + ' |  ' +\
              pll_str[2] + ' |  ' +\
              pll_str[3] + ' |  ' +\
              pll_str[4] + ' |  ' +\
              pll_str[5] + ' |  ' +\
              pll_str[6] + ' |  ' +\
              pll_str[7] + ' |  ' +\
              pll_str[8] 
    return pll_str

# Enter here the PLL value on canal 1
start = 82
# Set the Band:
band='A'
pll_value = start
for x in range(1, 41):
    channel = x
    if x == 22:
        print '| NOTE |CH. order | 22,| 24,| 25,| 23,| 26 | ...|    |    |    |        |'
    if x == 23:
        channel = 24
    if x == 24:
        channel = 25
    if x == 25:
        channel = 23
    print '|  %c   |    %02d    |  %s |  %d   |' %(band,channel,pll_value_to_str(pll_value),pll_value)
    if x in {3, 7, 11, 15, 19}:
        pll_value+=1
        print '|  %c   |    %02d bis|  %s |  %d   |' %(band,channel,pll_value_to_str(pll_value),pll_value)
    pll_value+=1
