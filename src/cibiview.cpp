/*
 * Copyright (c) 1998, Vincent Hervieux vincent.hervieux@gmail.com
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 * * Neither the name of the author Vincent Hervieux, nor the
 *   names of its contributors may be used to endorse or promote products
 *   derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "display.h"
#include "cibiview.h"

CibiView::CibiView(Adafruit_SSD1306 *_display) : View(_display),
                                                 display_(_display)
{
}

void CibiView::init()
{
  display_->clearDisplay();
  /* Top and bottom lines */
  display_->setTextSize(1);
  /* Top and bottom lines */
  display_->drawFastHLine(0, 0, SCREEN_WIDTH, WHITE);
  display_->drawFastHLine(0, SCREEN_HEIGHT - 1, SCREEN_WIDTH, WHITE);
}

void CibiView::setFreq(char *_frequency, uint16_t _color)
{
  const static int16_t FREQ_TEXT_POS_X PROGMEM = 35;
  const static int16_t FREQ_TEXT_POS_Y PROGMEM = 5;
  const static int16_t FREQ_UNIT_TEXT_POS_X PROGMEM = 108;
  const static int16_t FREQ_UNIT_TEXT_POS_Y PROGMEM = 12;
  display_->setTextColor(_color);
  display_->setTextSize(2);
  display_->setFont();
  display_->setCursor(FREQ_TEXT_POS_X, FREQ_TEXT_POS_Y);
  display_->print(_frequency[0]);
  display_->print(_frequency[1]);
  display_->print(_frequency[2]);
  display_->print(_frequency[3]);
  display_->print(_frequency[4]);
  display_->setTextSize(1);
  display_->setCursor(FREQ_UNIT_TEXT_POS_X, FREQ_UNIT_TEXT_POS_Y);
  display_->print(F("kHz"));
}

void CibiView::setCibiChannel(char *_channel, uint16_t _color)
{
  const static int16_t CHANNEL_TEXT_POS_X PROGMEM = 40;
  const static int16_t CHANNEL_TEXT_POS_Y PROGMEM = 55;
  display_->setTextColor(_color);
  display_->setTextSize(1);
  display_->setFont(&FreeSansBold24pt7bNumeric);
  if (_channel[0] == '0')
  {
    display_->setCursor(CHANNEL_TEXT_POS_X + FreeSansBold24pt7bNumeric.glyph[0].xAdvance, CHANNEL_TEXT_POS_Y);
    display_->print(_channel + 1);
  }
  else
  {
    display_->setCursor(CHANNEL_TEXT_POS_X, CHANNEL_TEXT_POS_Y);
    display_->print(_channel);
  }
}

void CibiView::setCibiChannelBis(bool _bis, uint16_t _color)
{
  const static int16_t BIS_TEXT_POS_X PROGMEM = 108;
  const static int16_t BIS_TEXT_POS_Y PROGMEM = 32;
  display_->setTextColor(_color);
  display_->setTextSize(1);
  display_->setFont();
  display_->setCursor(BIS_TEXT_POS_X, BIS_TEXT_POS_Y);
  if (_bis)
  {
    display_->print(F("bis"));
  }
  else
  {
    display_->print(F("   "));
  }
}

void CibiView::setCibiBand(char *_band, uint16_t _color)
{
  display_->setTextColor(_color);
  display_->setTextSize(2);
  display_->setFont();
  display_->setCursor(5, 32);
  display_->print(F("CB"));
}
