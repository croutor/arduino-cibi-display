/*
 * Copyright (c) 1998, Vincent Hervieux vincent.hervieux@gmail.com
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 * * Neither the name of the author Vincent Hervieux, nor the
 *   names of its contributors may be used to endorse or promote products
 *   derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
////
//// Display: using an OLED SSD1306
////
#include "displaycontroller.h"
#include "display.h"
#include "log.h"
#include <Wire.h>

#define OLED_RESET -1 // -1 = no reset pin

DisplayController::DisplayController() : display_(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET),
                                         first_display_(true),
                                         current_modulation_(MOD_CW),
                                         current_cibi_channel_({.frequency = 0,
                                                                .channel = 0,
                                                                .band = CIBI_BAND_UNKNOWN,
                                                                .bis = false}),
                                         cibi_view_(&display_),
                                         main_view_(&display_),
                                         view_(&main_view_) // set default view to main view
{
}

DisplayController::~DisplayController()
{
  display_.clearDisplay();
}

void DisplayController::init()
{
  const static byte I2C_OLED_ADDRESS_ PROGMEM = 0x3c;
  // initialize the Display I2C address (for the 128x64)
  display_.begin(SSD1306_SWITCHCAPVCC, I2C_OLED_ADDRESS_);
  view_->init();
}

void DisplayController::invertDisplay(boolean i)
{
  display_.invertDisplay(i);
}

void DisplayController::setFreq(uint32_t _freq, uint16_t _color)
{
  const static uint16_t MAX_FREQ_STR_LENGTH PROGMEM = 8; /* 30000000*/
  char str[MAX_FREQ_STR_LENGTH + 1] = {0};
  snprintf(str, MAX_FREQ_STR_LENGTH + 1, "%08lu", _freq);
  view_->setFreq(str, _color);
}

void DisplayController::setModulation(int _modulation, uint16_t _color)
{
  const static char mod_cw[] PROGMEM = "CW";
  const static char mod_am[] PROGMEM = "AM";
  const static char mod_fm[] PROGMEM = "FM";
  const static char mod_usb[] PROGMEM = "USB";
  const static char mod_lsb[] PROGMEM = "LSB";
  const static char *const modulations[] PROGMEM = {
      mod_cw, mod_am, mod_fm, mod_usb, mod_lsb};
  char str[4] = {0};
  strcpy_P(str, (char *)pgm_read_word(&(modulations[_modulation])));
  view_->setModulation(str, _color);
}

void DisplayController::setCibiChannel(int _channel, uint16_t _color)
{
  char str[3] = {0};
  if (-1 != _channel)
  {
    snprintf(str, sizeof(str), "%02d", _channel);
    view_->setCibiChannel(str, _color);
  }
}

void DisplayController::setCibiChannelBis(bool _bis, uint16_t _color)
{
  view_->setCibiChannelBis(_bis, _color);
}

void DisplayController::setCibiBand(int _band, uint16_t _color)
{
  const static char infinfinf[] PROGMEM = "---";
  const static char infinf[] PROGMEM = " --";
  const static char inf[] PROGMEM = "  -";
  const static char norm[] PROGMEM = "";
  const static char sup[] PROGMEM = "  +";
  const static char supsup[] PROGMEM = " ++";
  const static char supsupsup[] PROGMEM = "+++";
  const static char *const cibi_bands[] PROGMEM = {
      infinfinf, infinf, inf, norm, sup, supsup, supsupsup};

  char str[4] = {0};
  if (CIBI_BAND_UNKNOWN != _band)
  {
    strcpy_P(str, (char *)pgm_read_word(&(cibi_bands[_band])));
    view_->setCibiBand(str, _color);
  }
}

void DisplayController::setView(display_type_t _view)
{
  View *current_view_ = view_;
  switch (_view)
  {
  case DISPLAY_MAIN:
    view_ = &main_view_;
    break;
  case DISPLAY_CIBI:
    view_ = &cibi_view_;
    break;
  case DISPLAY_CONFIG:
    //view_ = &config_view_;
    break;
  default:
    /* No change */
    break;
  }
  if (current_view_ != view_)
  {
    view_->init();
    setModulation(current_modulation_, WHITE);
    setCibiChannel(current_cibi_channel_.channel, WHITE);
    setCibiChannelBis(current_cibi_channel_.bis, WHITE);
    /* frequency and band will likely change, so no need to set them */
  }
}

void DisplayController::updateChannel(cibi_channel_t *_channel)
{
  bool update = false;
  if (first_display_)
  {
    init();
    /* Never initialize again */
    first_display_ = false;
  }
  switch (_channel->band)
  {
    case CIBI_BAND_UNKNOWN:
      return;
    case CIBI_BAND_NORMAL:
      setView(DISPLAY_CIBI);
      break;
    default:
      setView(DISPLAY_MAIN);
      break;
  }

  if (current_cibi_channel_.frequency != _channel->frequency)
  {
    setFreq(current_cibi_channel_.frequency, BLACK);
    setFreq(_channel->frequency, WHITE);
    current_cibi_channel_.frequency = _channel->frequency;
    update = true;
  }
  if (current_cibi_channel_.band != _channel->band)
  {
    setCibiBand(current_cibi_channel_.band, BLACK);
    setCibiBand(_channel->band, WHITE);
    current_cibi_channel_.band = _channel->band;
    update = true;
  }
  if (current_cibi_channel_.channel != _channel->channel)
  {
    setCibiChannel(current_cibi_channel_.channel, BLACK);
    setCibiChannel(_channel->channel, WHITE);
    current_cibi_channel_.channel = _channel->channel;
    update = true;
  }
  if (current_cibi_channel_.bis != _channel->bis)
  {
    setCibiChannelBis(current_cibi_channel_.bis, BLACK);
    setCibiChannelBis(_channel->bis, WHITE);
    current_cibi_channel_.bis = _channel->bis;
    update = true;
  }
  if (update)
  {
    display_.display();
  }
}

void DisplayController::updateModulation(int _modulation)
{
  if (first_display_)
  {
    init();
    /* Never initialize again */
    first_display_ = false;
  }

  if (_modulation != current_modulation_)
  {
    setModulation(current_modulation_, BLACK);
    setModulation(_modulation, WHITE);
    current_modulation_ = _modulation;
    display_.display();
  }
}
