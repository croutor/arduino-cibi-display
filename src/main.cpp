/*
 * Copyright (c) 1998, Vincent Hervieux vincent.hervieux@gmail.com
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 * * Neither the name of the author Vincent Hervieux, nor the
 *   names of its contributors may be used to endorse or promote products
 *   derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/***
 *** Main entry point
 ***/
#include <Arduino.h>
#include "cibi_channels.h"
#include "displaycontroller.h"
#include "log.h"

/* set this flag when the PLL read is fake: */
//#define DEMO
/* set this flag to enable contrast revert feature. */
/* this requires an additional jumper or switch:    */
//#define CONTRAST
/* set this flag to optimize a bit the way register are read: */
#define OPTIMIZED

/* Modulation pins */
#define PIN_CW   13
#define PIN_AM   12
#define PIN_FM   11
#define PIN_USB  10
#define PIN_LSB   9
/* PLL status reading */
#define PIN_PLL8  8
#define PIN_PLL7  7
#define PIN_PLL6  6
#define PIN_PLL5  5
#define PIN_PLL4  4
#define PIN_PLL3  3
#define PIN_PLL2  2
#define PIN_PLL1 A1
#define PIN_PLL0 A0
/* Contrast pin */
#ifdef CONTRAST
#define PIN_CONTRAST A2
#endif
/*********************************/
/* OLED is using pins A4 and A5  */
/*********************************/
/* band selector needed on Grant */
#define PIN_BAND_LOW A3
#define PIN_BAND_MID A6
#define PIN_BAND_HI  A7

context_t context = {
    .modulation = MOD_NONE, /* radio modulation currently in use */
    .modulation_update = false,
    .contrast = false,
    .contrast_update = false,
    .pll = 0xFFFF,
    /* For now there is no configuration menu,
     * you need to set your cibi radio model below
     */
    .cibi_model = CIBI_MODEL_JACKSON,
    .channel = {
        .frequency = 0,
        .channel = 0,
        .band = CIBI_BAND_UNKNOWN,
        .bis = false
      },
    .channel_update = false
    };

DisplayController controller;
unsigned long currentMillis = 0;                  // stores the value of millis() in each iteration of loop()
unsigned long previousMillis = 0;                 // will store last time the OLED was updated
const unsigned long intervalMillis PROGMEM = 100; // OLED refresh period

/* Arduino setup */
void setup()
{
  /* Modulation swith pins */
  pinMode(PIN_CW, INPUT);
  pinMode(PIN_AM, INPUT);
  pinMode(PIN_FM, INPUT);
  pinMode(PIN_USB, INPUT);
  pinMode(PIN_LSB, INPUT);
  /* PLL status reading */
  pinMode(PIN_PLL8, INPUT);
  pinMode(PIN_PLL7, INPUT);
  pinMode(PIN_PLL6, INPUT);
  pinMode(PIN_PLL5, INPUT);
  pinMode(PIN_PLL4, INPUT);
  pinMode(PIN_PLL3, INPUT);
  pinMode(PIN_PLL2, INPUT);
  pinMode(PIN_PLL1, INPUT);
  pinMode(PIN_PLL0, INPUT);
#ifdef CONTRAST
  pinMode(PIN_CONTRAST, INPUT); /* contrast PIN */
#endif
  if(context.cibi_model == CIBI_MODEL_GRANT)
  {
    /* Those pins are active on low level */
    pinMode(PIN_BAND_LOW, INPUT_PULLUP); /* LOW */
    pinMode(PIN_BAND_MID, INPUT_PULLUP); /* MID */
    pinMode(PIN_BAND_HI, INPUT_PULLUP); /* HI */
  }

#ifdef DEBUG
  /* Debug console */
  Serial.begin(9600);
#endif
}

int readModulation(int _current_modulation)
{
#ifdef OPTIMIZED
  /* This reads D register only once instead of several calls to DigitalRead */
  int val = PINB; /* D8 to D13 */
  if (val & B00000010)
    return MOD_LSB; /*D9*/
  else if (val & B00000100)
    return MOD_USB; /*D10*/
  else if (val & B00001000)
    return MOD_FM; /*D11*/
  else if (val & B00010000)
    return MOD_AM; /*D12*/
  else if (val & B00100000)
    return MOD_CW; /*D13*/
  else
    return _current_modulation; /* DEFAULT: UNCHANGED */
#else
  if (HIGH == digitalRead(PIN_USB))
    return MOD_USB;
  else if (HIGH == digitalRead(PIN_AM))
    return MOD_AM;
  else if (HIGH == digitalRead(PIN_FM))
    return MOD_FM;
  else if (HIGH == digitalRead(PIN_LSB))
    return MOD_LSB;
  else if (HIGH == digitalRead(PIN_CW))
    return MOD_CW;
  else
    return _current_modulation; /* DEFAULT: UNCHANGED */
#endif
}

#ifdef DEMO
uint16_t pll = 0;
#endif
uint16_t readPLL(uint16_t _current_pll)
{
#ifndef DEMO
#ifdef OPTIMIZED
uint16_t pll = (PIND & B11111100) + /* D2 to D7 */
                 (PINC & B00000011);  /* A0 to A1 */
if (PINB & B00000001)
{
  /* D8 may add 256 */
  pll += 256;
}
#else
uint16_t pll = (digitalRead(PIN_PLL8) << 8) +
               (digitalRead(PIN_PLL7) << 7) +
               (digitalRead(PIN_PLL6) << 6) +
               (digitalRead(PIN_PLL5) << 5) +
               (digitalRead(PIN_PLL4) << 4) +
               (digitalRead(PIN_PLL3) << 3) +
               (digitalRead(PIN_PLL2) << 2) +
               (digitalRead(PIN_PLL1) << 1) +
               (digitalRead(PIN_PLL0) << 0);
#endif
#else
  /* DEMO MODE */
  switch (context.cibi_model)
  {
  case CIBI_MODEL_360FM:
    /* Fake a PLL read of a Superstar 360FM: */
    pll = (pll++ % 135) + 82;
    break;
  case CIBI_MODEL_3900:
    /* Fake a PLL read of a Superstar 3900: */
    pll = (pll++ % 271) + 91;
    break;
  case CIBI_MODEL_JACKSON:
    /* Fake a PLL read of a President Jackson: */
    pll = (pll++ % 226) + 82;
    break;
  default:
    pll = 1;
  }
#endif
  /* check PLL value read is within a valid range.
   * Why? Because while rotating the channel selector,
   * values read might be out of boundaries
   */
  if(pll < pgm_read_word(pll_min_by_model + context.cibi_model) ||
     pll > pgm_read_word(pll_max_by_model + context.cibi_model)) {
       /* invalid read, return unchanged value */
       pll = _current_pll;
  }
  return pll;
}

void computeCibiChannelFromPLL(context_t *_ctx)
{
  /* Cibi channels */
  /* Pay attention that TX over 28Mhz is reserved for HAM use */
  const static uint16_t FREQ_CIBI_LOW_DAKHZ PROGMEM = 2561;
  /* Round to kHz to compute channels
     * This avoids to get no display of channels
     * in case a fine tuning was made to clarify the voice
     */
  switch (_ctx->cibi_model)
  {
  case CIBI_MODEL_360FM:
    /* PLL starts at 82 and is inf  band */
    _ctx->pll += 8;
    break;
  case CIBI_MODEL_3900:
    /* PLL starts at 91 and is inf inf inf  band */
    _ctx->pll -= 91;
    break;
  case CIBI_MODEL_JACKSON:
    /* PLL starts at 82 and is inf inf  band */
    _ctx->pll -= 37;
    break;
  case CIBI_MODEL_GRANT:
    /* PLL starts at 82 on all bands */
    if(LOW == digitalRead(PIN_BAND_LOW))
    {
      /* INF */
      _ctx->pll += 8;
    }
    else if(LOW == digitalRead(PIN_BAND_MID))
    {
      /* MID */
      _ctx->pll += 53;
    }
    else if(LOW == digitalRead(PIN_BAND_HI))
    {
      /* HI */
      _ctx->pll += 98;
    }
    /* TODO: GRANT: need to read band selector! */
    break;
  case CIBI_MODEL_ALAN88S:
    /* 246 is the canal 1 on cibi band,
       and on this cibi, when the channel increase, the PLL is decreasing */
    _ctx->pll = 135 + 246 - _ctx->pll;
    break;
  default:
    /* Do not change PLL value, unsupported model! */
    break;
  }
  uint16_t channel_index = _ctx->pll % CIBI_BAND_DAKHZ;
  _ctx->channel.band = _ctx->pll / CIBI_BAND_DAKHZ;
  if (_ctx->channel.band > CIBI_BAND_MAX ||
      _ctx->channel.band < CIBI_BAND_UNKNOWN)
  {
    _ctx->channel.band = CIBI_BAND_UNKNOWN;
  }
  else
  {
    _ctx->channel.channel = pgm_read_byte_near(cibi_channels + channel_index);
    _ctx->channel.bis = pgm_read_byte_near(cibi_channels_bis + channel_index);
    _ctx->channel.frequency = FREQ_CIBI_LOW_DAKHZ;
    _ctx->channel.frequency += _ctx->pll;
    _ctx->channel.frequency *= 10000;
    _ctx->channel.frequency += 5000;
  }
#ifdef DEBUG
  Serial.print(F("Cibi band: "));
  Serial.println(_ctx->channel.band);
  Serial.print(F("Cibi channel: "));
  Serial.println(_ctx->channel.channel);
  Serial.print(F("Cibi bis: "));
  Serial.println(_ctx->channel.bis);
  Serial.print(F("Cibi freq: "));
  Serial.println(_ctx->channel.frequency);
#endif
}

/* Arduino main loop */
void loop()
{
  uint16_t prev_pll = context.pll;
  int prev_modulation = context.modulation;
#ifdef CONTRAST
  bool prev_contrast = context.contrast;
#endif

  currentMillis = millis();

  /* this read the input pins */
  context.pll = readPLL(prev_pll);
  if (prev_pll != context.pll)
  {
    /* pay attention that channel needs to be computed prior to being displayed */
    context.channel_update = true;
  }
  context.modulation = readModulation(prev_modulation);
  if (prev_modulation != context.modulation)
  {
    context.modulation_update = true;
  }
#ifdef CONTRAST
  context.contrast = digitalRead(PIN_CONTRAST);
  if (prev_contrast != context.contrast)
  {
    context.contrast_update = true;
  }
#endif

  /* this will update periodically the OLED screen */
  if (currentMillis - previousMillis >= intervalMillis) {
    previousMillis += intervalMillis;
    // TODO update OLED
    if(context.channel_update) {
      computeCibiChannelFromPLL(&context);
      controller.updateChannel(&context.channel);
      context.channel_update = false;
    }
    if(context.modulation_update) {
      controller.updateModulation(context.modulation);
      context.modulation_update = false;
    }
    if(context.contrast_update) {
      controller.invertDisplay(context.contrast);
      context.contrast_update = false;
    }
  }
}
