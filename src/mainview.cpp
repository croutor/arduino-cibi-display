/*
 * Copyright (c) 1998, Vincent Hervieux vincent.hervieux@gmail.com
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 * * Neither the name of the author Vincent Hervieux, nor the
 *   names of its contributors may be used to endorse or promote products
 *   derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "display.h"
#include "mainview.h"

MainView::MainView(Adafruit_SSD1306 *_display) : View(_display),
                                                 display_(_display)
{
}

void MainView::init()
{
  const static int16_t FREQ_DOT_TEXT_POS_X PROGMEM = 50;
  const static int16_t MHZ_TEXT_POS_X PROGMEM = 108;
  const static int16_t MHZ_TEXT_POS_Y PROGMEM = 38;
  display_->clearDisplay();
  /* Mhz sign */
  display_->setTextColor(WHITE);
  display_->setTextSize(1);
  display_->setFont();
  display_->setCursor(MHZ_TEXT_POS_X, MHZ_TEXT_POS_Y);
  display_->print(F("MHz"));
  /* Top and bottom lines */
  display_->drawFastHLine(0, 0, SCREEN_WIDTH, WHITE);
  display_->drawFastHLine(0, SCREEN_HEIGHT - 1, SCREEN_WIDTH, WHITE);
  /* Frequency dot */
  display_->fillCircle(FREQ_DOT_TEXT_POS_X, FREQ_TEXT_POS_Y, 2, WHITE);
}

void MainView::setFreq(char *_frequency, uint16_t _color)
{
  const static int16_t FREQ_BIG_TEXT_POS_X PROGMEM = 11;
  const static int16_t FREQ_SMALL_TEXT_POS_X PROGMEM = 54;
  char big_str[4] = {0};
  /* MHz string */
  strncpy(big_str, _frequency, 2 * sizeof(char));
  display_->setTextSize(1);
  display_->setFont(&FreeSansBold18pt7bNumeric);
  display_->setCursor(FREQ_BIG_TEXT_POS_X, FREQ_TEXT_POS_Y);
  display_->setTextColor(_color);
  display_->print(big_str);
  display_->setCursor(FREQ_SMALL_TEXT_POS_X, FREQ_TEXT_POS_Y);
  // display_->setFont(&FreeSansBold12pt7bNumeric);
  // display_->print(_frequency + 2 * sizeof(char));
  /* KHz string */
  strncpy(big_str, _frequency + 2, 3 * sizeof(char));
  display_->print(big_str);
}

void MainView::setCibiChannel(char *_channel, uint16_t _color)
{
  const static int16_t CIBI_CHANNEL_TEXT_POS_X PROGMEM = 44;
  const static int16_t CIBI_CHANNEL_TEXT_POS_Y PROGMEM = 58;
  display_->setTextSize(1);
  display_->setFont(&FreeSansBold18pt7bNumeric);
  display_->setCursor(CIBI_CHANNEL_TEXT_POS_X, CIBI_CHANNEL_TEXT_POS_Y);
  display_->setTextColor(_color);
  if (_channel[0] == '0')
  {
    display_->setCursor(CIBI_CHANNEL_TEXT_POS_X + FreeSansBold18pt7bNumeric.glyph[0].xAdvance, CIBI_CHANNEL_TEXT_POS_Y);
    display_->print(_channel + 1);
  }
  else
  {
    display_->setCursor(CIBI_CHANNEL_TEXT_POS_X, CIBI_CHANNEL_TEXT_POS_Y);
    display_->print(_channel);
  }
}

void MainView::setCibiChannelBis(bool _bis, uint16_t _color)
{
  const static int16_t CIBI_CHANNEL_BIS_TEXT_POS_X PROGMEM = 84;
  const static int16_t CIBI_CHANNEL_BIS_POS_Y PROGMEM = 43;
  if (_bis)
  {
    display_->setTextSize(1);
    display_->setFont();
    display_->setCursor(CIBI_CHANNEL_BIS_TEXT_POS_X, CIBI_CHANNEL_BIS_POS_Y);
    display_->setTextColor(_color);
    display_->print(F("bis"));
  }
}

void MainView::setCibiBand(char *_band, uint16_t _color)
{
  const int16_t CIBI_BAND_TEXT_POS_X PROGMEM = 2;
  const int16_t CIBI_BAND_TEXT_POS_Y PROGMEM = 40;
  display_->setTextSize(2);
  display_->setFont();
  display_->setCursor(CIBI_BAND_TEXT_POS_X, CIBI_BAND_TEXT_POS_Y);
  display_->setTextColor(_color);
  display_->print(_band);
}
