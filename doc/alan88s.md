# Midland Alan 88s PLL table

PLL mounted in this Cibi is the PLL02A or equivalent (AN6040). Table below is its truth table.

- Note : there are no 'bis' channels available on the cibi band for this tranceiver. Therefore they are marked with a 'X' below.
| channel | P8 | P7 | P6 | P5 | P4 | P3 | P2 | P1 | P0 | Dec |
|---------|----|----|----|----|----|----|----|----|----|-----|
|   -1    |  1 |  0 |  0 |  0 |  1 |  1 |  1 |  1 |  0 | 286 |
|   ...   |  . |  . |  . |  . |  . |  . |  . |  . |  . | ... |
|  -40    |  0 |  1 |  1 |  1 |  1 |  0 |  1 |  1 |  1 | 247 |
|    1    |  0 |  1 |  1 |  1 |  1 |  0 |  1 |  1 |  0 | 246 |
|    2    |  0 |  1 |  1 |  1 |  1 |  0 |  1 |  0 |  1 | 245 |
|    3    |  0 |  1 |  1 |  1 |  1 |  0 |  1 |  0 |  0 | 244 |
| X  3 bis|  0 |  1 |  1 |  1 |  1 |  0 |  0 |  1 |  1 | 243 |
|    4    |  0 |  1 |  1 |  1 |  1 |  0 |  0 |  1 |  0 | 242 |
|   ...   |  . |  . |  . |  . |  . |  . |  . |  . |  . | ... |
|   40    |  0 |  1 |  1 |  0 |  0 |  1 |  0 |  1 |  0 | 202 |
|   ...   |  . |  . |  . |  . |  . |  . |  . |  . |  . | ... |
|   50    |  0 |  1 |  1 |  0 |  0 |  0 |  0 |  0 |  0 | 192 |
|   ...   |  . |  . |  . |  . |  . |  . |  . |  . |  . | ... |
|  100    |  0 |  1 |  0 |  0 |  0 |  1 |  1 |  1 |  0 | 142 |
|   ...   |  . |  . |  . |  . |  . |  . |  . |  . |  . | ... |
|  200    |  0 |  0 |  0 |  1 |  0 |  1 |  0 |  1 |  0 |  42 |
